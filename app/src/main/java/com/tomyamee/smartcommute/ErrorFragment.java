package com.tomyamee.smartcommute;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by tommy on 12/07/2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class ErrorFragment extends Fragment {

    public static ErrorFragment newInstance() {
        return new ErrorFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_empty_live, container, false);
    }
}

package com.tomyamee.smartcommute;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.job.JobScheduler;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tommy on 01/28/2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class TrackSpeedService extends Service {
    private LocationManager locationManager;
    private LocationListener locationListener;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private LocationRequest mLocationRequest;

    private float avgSpeed;
    private int tripCount;
    private List<Float> currentTrackedSpeed;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Intent nIntent = new Intent();
        nIntent.setClass(getApplicationContext(), LiveDirectionActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, nIntent, 0);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel channel = new NotificationChannel("SmartCommute", "SmartCommute", NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);
            Notification notification = new Notification.Builder(this, "SmartCommute").setContentTitle("SmartCommute").setContentText("Tracking User Speed").setSmallIcon(R.drawable.train_notification).setContentIntent(pendingIntent).setTicker("Test").build();
            notification.flags = Notification.FLAG_AUTO_CANCEL;
            startForeground(1, notification);
        }
        else{
            Notification notification = new NotificationCompat.Builder(this).setContentTitle("SmartCommute").setContentText("Tracking User Speed").setSmallIcon(R.drawable.train_notification).setContentIntent(pendingIntent).setTicker("Test").build();
            notification.flags = Notification.FLAG_AUTO_CANCEL;
            startForeground(1, notification);
        }

        currentTrackedSpeed = new ArrayList<>();

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                for (Location location : locationResult.getLocations()) {

                }
            }
        };
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (checkLocationPermission()) {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
        }

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final FirebaseAuth auth = FirebaseAuth.getInstance();
        database.getReference().child("User").child(auth.getCurrentUser().getUid()).child("AverageSpeed").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null){
                    avgSpeed = 0;
                }
                else {
                    String avg = dataSnapshot.getValue().toString();
                    avgSpeed = Float.valueOf(avg);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        database.getReference().child("User").child(auth.getCurrentUser().getUid()).child("TripCount").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null){
                    tripCount = 0;
                }
                else {
                    String trip = dataSnapshot.getValue().toString();
                    tripCount = Integer.valueOf(trip);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                float speed = location.getSpeed();
                speed = ((speed * 60) * 60) / 1000;
                if (avgSpeed != 0){
                    if (speed < 7){
                        if (speed >= (avgSpeed - 1.5) && speed <= (avgSpeed + 1.5)){
                            currentTrackedSpeed.add(speed);
                        }
                    }
                }
                else{
                    if (speed < 10) {
                        if (currentTrackedSpeed.size() > 0 && speed == 0){
                            if (currentTrackedSpeed.get((currentTrackedSpeed.size() - 1)) != 0){
                                currentTrackedSpeed.add(speed);
                            }
                        }
                        else {
                            currentTrackedSpeed.add(speed);
                        }
                    }
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

        if (checkLocationPermission()) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100, 0, locationListener);
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        locationManager.removeUpdates(locationListener);
        endTrip();
        stopForeground(true);
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private boolean checkLocationPermission() {
        return ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private void endTrip(){
        int count = currentTrackedSpeed.size();
        float totalSpeed = 0;
        float currentAvgSpeed;
        if (count != 0) {
            for (float speed: currentTrackedSpeed){
                totalSpeed += speed;
            }
            currentAvgSpeed = totalSpeed / count;
            if (currentAvgSpeed != 0) {
                avgSpeed = ((avgSpeed * tripCount) + currentAvgSpeed) / (tripCount + 1);
                if (avgSpeed > 0) {
                    tripCount++;
                }
            }
        }
        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.cancelAll();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        FirebaseAuth auth = FirebaseAuth.getInstance();
        database.getReference().child("User").child(auth.getCurrentUser().getUid()).child("CurrentSpeed").setValue(0);
        DecimalFormat df = new DecimalFormat("0.00");
        if (avgSpeed != 0) {
            database.getReference().child("User").child(auth.getCurrentUser().getUid()).child("AverageSpeed").setValue(df.format(avgSpeed));
        }
        database.getReference().child("User").child(auth.getCurrentUser().getUid()).child("TripCount").setValue(tripCount);
        DatabaseReference ref = database.getReference().child("User").child(auth.getCurrentUser().getUid()).child("ScheduleTrip");
        ref.removeValue();
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }
}

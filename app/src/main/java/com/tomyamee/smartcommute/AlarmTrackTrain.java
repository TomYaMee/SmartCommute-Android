package com.tomyamee.smartcommute;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by tommy on 02/07/2018.
 */

public class AlarmTrackTrain extends BroadcastReceiver {
    @Override
    public void onReceive(final Context context, Intent intent) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        wl.acquire();

        System.out.println("[Debug] Checking");

        FirebaseApp.initializeApp(context);
        final FirebaseAuth auth = FirebaseAuth.getInstance();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        DatabaseReference tripDetail = database.getReference().child("User").child(auth.getCurrentUser().getUid()).child("ScheduleTrip").child("Trip");
        tripDetail.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    String direction;
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        direction = dataSnapshot1.child("Direction").getValue().toString();
                        if (direction.contains("Train towards")) {
                            sendNotification("Train", direction, context, 0);
                        }
                        else if (direction.contains("Subway towards")){
                            sendNotification("Subway", direction, context, 1);
                        }
                        else if (direction.contains("Monorail towards")){
                            sendNotification("Monorail", direction, context, 2);
                        }
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        wl.release();
    }

    private void sendNotification(final String line, String direction, final Context context, final int id){
        final String station;
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference stationName;
        if (line.equalsIgnoreCase("Train")) {
            station = direction.substring(14);
            stationName = database.getReference().child("Train").child("KTM").child(station);
        }
        else if (line.equalsIgnoreCase("Subway")){
            station = direction.substring(18);
            stationName = database.getReference().child("Train").child("LRT").child(station);
        }
        else{
            station = direction.substring(20);
            stationName = database.getReference().child("Train").child("Monorail").child(station);
        }
        stationName.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.child("Status").getValue() != null) {
                    String status = dataSnapshot.child("Status").getValue().toString();
                    if (!status.equalsIgnoreCase("Operational")) {
                        Intent nIntent = new Intent();
                        nIntent.setClass(context, MainActivity.class);
                        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, nIntent, 0);

                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                            NotificationChannel channel = new NotificationChannel("SmartCommute", "SmartCommute", NotificationManager.IMPORTANCE_HIGH);
                            notificationManager.createNotificationChannel(channel);
                            Notification notification = new Notification.Builder(context, "SmartCommute").setContentTitle("SmartCommute").setContentText(line + " Line Experiencing Delay").setSmallIcon(R.drawable.train_notification).setContentIntent(pendingIntent).setTicker("Train Experiencing Delay").build();
                            notification.flags = Notification.FLAG_AUTO_CANCEL | Notification.FLAG_ONLY_ALERT_ONCE;
                            notificationManager.notify(id, notification);
                        } else {
                            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                            Notification notification = new NotificationCompat.Builder(context).setContentTitle("SmartCommute").setContentText(line + " Line Experiencing Delay").setSmallIcon(R.drawable.train_notification).setContentIntent(pendingIntent).setTicker("Train Experiencing Delay").build();
                            notification.flags = Notification.FLAG_AUTO_CANCEL | Notification.FLAG_ONLY_ALERT_ONCE;
                            notificationManager.notify(id, notification);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setAlarm(Context context){
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, AlarmTrackTrain.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000, pi);
    }

    public void cancelAlarm(Context context){
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, AlarmTrackTrain.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        am.cancel(pi);
    }
}

package com.tomyamee.smartcommute;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by tommy on 02/01/2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class TrackTrainService extends Service {
    AlarmTrackTrain alarm = new AlarmTrackTrain();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("[Debug] Starting service");
        alarm.setAlarm(this);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        System.out.println("[Debug] Ending service");
        alarm.cancelAlarm(this);
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}



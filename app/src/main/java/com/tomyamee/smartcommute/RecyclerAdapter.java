package com.tomyamee.smartcommute;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by tommy on 01/27/2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.Holder> {

    private List<DirectionData> directionData;

    public RecyclerAdapter(List<DirectionData> directionData) {
        this.directionData = directionData;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.livedirection_list, null);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.textDirection.setText(directionData.get(position).getmDirection());
        holder.textDistance.setText(directionData.get(position).getmDistance());
        holder.textDuration.setText(directionData.get(position).getmDuration());
        holder.textIcon.setText(directionData.get(position).getmIcon());
    }

    @Override
    public int getItemCount() {
        return directionData.size();
    }

    public static class Holder extends RecyclerView.ViewHolder {

        final TextView textIcon;
        final TextView textDirection;
        final TextView textDistance;
        final TextView textDuration;

        public Holder(View view) {
            super(view);
            textIcon = view.findViewById(R.id.textLiveIcon);
            textDirection = view.findViewById(R.id.textLiveDirection);
            textDistance = view.findViewById(R.id.textLiveDistance);
            textDuration = view.findViewById(R.id.textLiveDuration);

        }
    }
}

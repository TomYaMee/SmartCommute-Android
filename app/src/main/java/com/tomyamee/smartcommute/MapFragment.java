package com.tomyamee.smartcommute;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.GeoDataApi;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBufferResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.android.PolyUtil;
import com.google.maps.model.*;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Instant;

import java.net.URL;
import java.util.Calendar;
import java.util.List;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * Created by tommy on 12/07/2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class MapFragment extends Fragment {

    private static final int REQUEST_PERMISSION_LOCATION = 1001;
    private static final int ORIGIN_PLACE_SEARCH_REQUEST_CODE = 1003;
    private static final int DESTINATION_PLACE_SEARCH_REQUEST_CODE = 1004;

    //Map Layout
    private MapView mMapView;
    private GoogleMap mgoogleMap;
    private Marker mMarkerOrigin;
    private Marker mMarkerDestination;
    private Polyline mDirectionLine;

    //Schedule Layout
    private Spinner mSpinner;
    private TextView mDepartArriveText;
    private EditText mOrigin;
    private EditText mDestination;
    private ImageButton mCurrentPin;
    private Button mNavigationSearch;
    private Button mStartDirection;

    //Local Variable
    private DirectionsResult mResult;
    private LatLng mDefaultLocation;
    private Authentication mAuth;
    private FirebaseDatabase mDatabase;
    private ArrayAdapter<CharSequence> mAdapter;
    private Instant mDepartDateTime;
    private LatLng mDestinationLatLng;
    private LatLng mOriginLatLng;

    //Database Reference
    private DatabaseReference originRef;
    private DatabaseReference destinationRef;
    private DatabaseReference departTypeRef;
    private DatabaseReference departTimeRef;
    private DatabaseReference encodedPathRef;

    //Google API
    private GeoDataClient geoDataClient;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private LocationRequest mLocationRequest;
    private String mOriginId;
    private String mDestinationId;


    public static MapFragment newInstance() {
        return new MapFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                for (Location location : locationResult.getLocations()) {
                    if (mDefaultLocation == null) {
                        mDefaultLocation = new LatLng(location.getLatitude(), location.getLongitude());
                        if (mgoogleMap != null) {
                            CameraPosition cameraPosition = new CameraPosition.Builder().target(mDefaultLocation).zoom(12).build();
                            mgoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }
                    }
                    else{
                        mDefaultLocation = new LatLng(location.getLatitude(), location.getLongitude());
                    }
                }
            }
        };
        mAuth = new Authentication((MainActivity) getActivity());
        geoDataClient = Places.getGeoDataClient(getActivity(), null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_map, container, false);

        mMapView = rootView.findViewById(R.id.mapVIew);
        mDepartArriveText = rootView.findViewById(R.id.departArriveTextMap);
        mSpinner = rootView.findViewById(R.id.timeDropdown);
        mOrigin = rootView.findViewById(R.id.originSearch);
        mCurrentPin = rootView.findViewById(R.id.imageButtonPin);
        mDestination = rootView.findViewById(R.id.destinationSearch);
        mNavigationSearch = rootView.findViewById(R.id.navigateButton);
        mStartDirection = rootView.findViewById(R.id.buttonStartDirection);
        mAdapter = ArrayAdapter.createFromResource(rootView.getContext(), R.array.time_array, android.R.layout.simple_spinner_dropdown_item);

        mDatabase = FirebaseDatabase.getInstance();

        mMapView.onCreate(savedInstanceState);
        mMapView.onResume(); // needed to get the map to display immediately
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        checkPermission();

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                mgoogleMap = mMap;

                // For showing a move to my location button
                if (checkLocationPermission()) {
                    mgoogleMap.setMyLocationEnabled(true);
                    mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                                if (mDefaultLocation == null) {
                                    mDefaultLocation = new LatLng(location.getLatitude(), location.getLongitude());
                                    CameraPosition cameraPosition = new CameraPosition.Builder().target(mDefaultLocation).zoom(12).build();
                                    mgoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                                }
                            }
                        }
                    });
                }
            }
        });

        new loadData().execute();

        return rootView;
    }

    private void checkPermission() {
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION_LOCATION);
        }
    }

    //Display message after granting permission
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), "Location permission granted!", Toast.LENGTH_SHORT).show();
                    if (checkLocationPermission()) {
                        mMapView.getMapAsync(new OnMapReadyCallback() {
                            @Override
                            public void onMapReady(GoogleMap mMap) {
                                mgoogleMap = mMap;

                                // For showing a move to my location button
                                if (checkLocationPermission()) {
                                    mgoogleMap.setMyLocationEnabled(true);
                                    mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                                        @Override
                                        public void onSuccess(Location location) {
                                            if (location != null) {
                                                if (mDefaultLocation == null) {
                                                    mDefaultLocation = new LatLng(location.getLatitude(), location.getLongitude());
                                                    CameraPosition cameraPosition = new CameraPosition.Builder().target(mDefaultLocation).zoom(12).build();
                                                    mgoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                                                }
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                } else {
                    Toast.makeText(getActivity(), "Permission needed to track your location!", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                Toast.makeText(getActivity(), "Test", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    //Clear mDatabase and clear fields on UI
    private void cancelSchedule() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        FirebaseAuth auth = FirebaseAuth.getInstance();
        DatabaseReference ref = database.getReference().child("User").child(auth.getCurrentUser().getUid()).child("ScheduleTrip");
        ref.removeValue();

        mDirectionLine.remove();

        mOrigin.setText("Current Location");
        mOrigin.setEnabled(true);

        mDestination.setText("");
        mDestination.setEnabled(true);

        mSpinner.setEnabled(true);
        mStartDirection.setVisibility(View.INVISIBLE);

        mDepartDateTime = null;
        mDepartArriveText.setText("Tap Me to Set Time");

        mMarkerDestination.remove();
        mMarkerOrigin.remove();

        ((MainActivity)getActivity()).cancelJob();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (checkLocationPermission()) {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    private void timePicker() {
        final Calendar c = Calendar.getInstance();
        final int hour = c.get(Calendar.HOUR_OF_DAY);
        final int minute = c.get(Calendar.MINUTE);
        final int year = c.get(Calendar.YEAR);
        final int month = c.get(Calendar.MONTH);
        final int day = c.get(Calendar.DAY_OF_MONTH);

        TimePickerDialog timePickerDialog = new TimePickerDialog(getView().getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int lHour, int lMinute) {


                if (lHour < hour) {
                    Toast.makeText(getContext(), "Past time selected!", Toast.LENGTH_SHORT).show();
                } else if (lMinute < minute && lHour == hour) {
                    Toast.makeText(getContext(), "Past time selected!", Toast.LENGTH_SHORT).show();
                } else {
                    mDepartDateTime = convertTimeToInstant(year, month + 1, day, lHour, lMinute);
                    mDepartArriveText.setText(lHour + ":" + lMinute);
                }
            }
        }, hour, minute, DateFormat.is24HourFormat(getActivity()));
        timePickerDialog.show();
    }

    private void launchPlaceSearch(View view, String tag) {
        try {
            AutocompleteFilter filter = new AutocompleteFilter.Builder().setCountry("MY").build();
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).setFilter(filter).build(getActivity());

            if (tag.equalsIgnoreCase("Origin"))
                startActivityForResult(intent, ORIGIN_PLACE_SEARCH_REQUEST_CODE);
            else
                startActivityForResult(intent, DESTINATION_PLACE_SEARCH_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {

        } catch (GooglePlayServicesNotAvailableException e) {

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ORIGIN_PLACE_SEARCH_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getContext(), data);
                System.out.println("Place: " + place.getName());
                mOrigin.setText(place.getName());
                mOriginLatLng = place.getLatLng();
                mOriginId = place.getId();
                if (mMarkerOrigin != null) {
                    mMarkerOrigin.remove();
                }
                if (mDirectionLine != null) {
                    mDirectionLine.remove();
                }
                mMarkerOrigin = mgoogleMap.addMarker(new MarkerOptions().position(mOriginLatLng));
                CameraPosition cameraPosition = new CameraPosition.Builder().target(mOriginLatLng).zoom(12).build();
                mgoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getContext(), data);
                System.out.println(status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {


            }
        } else if (requestCode == DESTINATION_PLACE_SEARCH_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getContext(), data);
                System.out.println("Place: " + place.getName());
                mDestination.setText(place.getName());
                mDestinationLatLng = place.getLatLng();
                mDestinationId = place.getId();
                if (mMarkerDestination != null) {
                    mMarkerDestination.remove();
                }
                if (mDirectionLine != null) {
                    mDirectionLine.remove();
                }
                mMarkerDestination = mgoogleMap.addMarker(new MarkerOptions().position(mDestinationLatLng));
                CameraPosition cameraPosition = new CameraPosition.Builder().target(mDestinationLatLng).zoom(12).build();
                mgoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getContext(), data);
                System.out.println(status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {


            }
        }
    }

    private boolean checkLocationPermission() {
        return ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    //Convert to unix as google direction uses Unix for departure and arrival time
    private Instant convertTimeToInstant(int year, int month, int day, int hour, int minute) {
        DateTimeZone dateTimeZone = DateTimeZone.forID("Asia/Kuala_Lumpur");
        DateTime dateTime = new DateTime(year, month, day, hour, minute, 0, 0, dateTimeZone);

        return dateTime.toInstant();
    }

    private void addMarker(final String destination1){
        try {
            geoDataClient.getPlaceById(destination1).addOnCompleteListener(new OnCompleteListener<PlaceBufferResponse>() {
                @Override
                public void onComplete(@NonNull Task<PlaceBufferResponse> task) {
                    PlaceBufferResponse places = task.getResult();
                    Place destination = places.get(0);
                    mDestination.setText(destination.getName());
                    if (mMarkerDestination != null) {
                        try {
                            mMarkerDestination.remove();
                            mMarkerDestination = mgoogleMap.addMarker(new MarkerOptions().position(destination.getLatLng()));
                        }
                        catch (Exception e){
                            addMarker(destination1);
                        }
                    } else {
                        try {
                            mMarkerDestination = mgoogleMap.addMarker(new MarkerOptions().position(destination.getLatLng()));
                        }
                        catch (Exception e){
                            addMarker(destination1);
                        }
                    }
                    places.release();
                }
            });
        }
        catch(Exception e){
            addMarker(destination1);
        }
    }

    //Launchs direction search asynchronously
    private class DirectionAPI extends AsyncTask<URL, Integer, Long> {

        private String destination;
        private String origin;
        private com.google.maps.model.LatLng localOrigin;
        private com.google.maps.model.LatLng localDestination;
        private boolean error = false;
        ProgressDialog progressDialog = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            progressDialog.setTitle("Searching");
            progressDialog.setMessage("Please wait while the system searches for the most optimal route");
            progressDialog.setCancelable(false);
            progressDialog.show();
            destination = mDestination.getText().toString();
            origin = mOrigin.getText().toString();
            try {
                if (mOrigin.getText().toString().equalsIgnoreCase("Current Location")){
                    localOrigin = new com.google.maps.model.LatLng(mDefaultLocation.latitude, mDefaultLocation.longitude);
                }
                else {
                    geoDataClient.getPlaceById(mOriginId).addOnCompleteListener(new OnCompleteListener<PlaceBufferResponse>() {
                        @Override
                        public void onComplete(@NonNull Task<PlaceBufferResponse> task) {
                            if (task.isSuccessful()) {
                                PlaceBufferResponse places = task.getResult();
                                Place myPlace = places.get(0);
                                localOrigin = new com.google.maps.model.LatLng(myPlace.getLatLng().latitude, myPlace.getLatLng().longitude);
                                places.release();
                            } else {

                            }
                        }
                    });
                }
                //localOrigin = new com.google.maps.model.LatLng(mOriginLatLng.latitude, mOriginLatLng.longitude);
            }
            catch (Exception e){
                Toast.makeText(getActivity(), "System currently could not get your current locaation, please try again later.", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                error = true;
                return;
            }
            geoDataClient.getPlaceById(mDestinationId).addOnCompleteListener(new OnCompleteListener<PlaceBufferResponse>() {
                @Override
                public void onComplete(@NonNull Task<PlaceBufferResponse> task) {
                    if (task.isSuccessful()) {
                        PlaceBufferResponse places = task.getResult();
                        Place myPlace = places.get(0);
                        localDestination = new com.google.maps.model.LatLng(myPlace.getLatLng().latitude, myPlace.getLatLng().longitude);
                        places.release();
                    } else {

                    }
                }
            });
            //localDestination = new com.google.maps.model.LatLng(mDestinationLatLng.latitude, mDestinationLatLng.longitude);
            if (mDirectionLine != null) {
                mDirectionLine.remove();
            }
        }

        @Override
        protected Long doInBackground(URL... urls) {
            if (!error) {
                try {
                    GeoApiContext context = new GeoApiContext.Builder().apiKey("AIzaSyBucrA-XR6nEOQ-AMmG3pObvR7oR3eEcYs").build();

                    mResult = null;
                    try {
                        if (mSpinner.getSelectedItem().toString().equalsIgnoreCase("Now")) {
                            if (origin.equalsIgnoreCase("Current Location")) {
                                mResult = DirectionsApi.newRequest(context).mode(TravelMode.TRANSIT).origin(new com.google.maps.model.LatLng(mDefaultLocation.latitude, mDefaultLocation.longitude)).destination(destination).transitMode(TransitMode.RAIL).alternatives(false).await();
                            } else {
                                mResult = DirectionsApi.newRequest(context).mode(TravelMode.TRANSIT).origin(localOrigin).destination(localDestination).transitMode(TransitMode.RAIL).alternatives(false).await();
                            }
                        } else if (mSpinner.getSelectedItem().toString().equalsIgnoreCase("Set Departure Time")) {
                            //DateTime time = new DateTime(mDepartDateTime);
                            if (mDepartDateTime != null) {
                                if (origin.equalsIgnoreCase("Current Location")) {
                                    mResult = DirectionsApi.newRequest(context).mode(TravelMode.TRANSIT).origin(new com.google.maps.model.LatLng(mDefaultLocation.latitude, mDefaultLocation.longitude)).destination(destination).transitMode(TransitMode.RAIL).departureTime(mDepartDateTime).alternatives(false).await();
                                } else {
                                    mResult = DirectionsApi.newRequest(context).mode(TravelMode.TRANSIT).origin(localOrigin).destination(localDestination).transitMode(TransitMode.RAIL).departureTime(mDepartDateTime).alternatives(false).await();
                                }
                            }
                        } else {
                            if (mDepartDateTime != null) {
                                if (origin.equalsIgnoreCase("Current Location")) {
                                    mResult = DirectionsApi.newRequest(context).mode(TravelMode.TRANSIT).origin(new com.google.maps.model.LatLng(mDefaultLocation.latitude, mDefaultLocation.longitude)).destination(destination).transitMode(TransitMode.RAIL).arrivalTime(mDepartDateTime).alternatives(false).await();
                                } else {
                                    mResult = DirectionsApi.newRequest(context).mode(TravelMode.TRANSIT).origin(localOrigin).destination(localDestination).transitMode(TransitMode.RAIL).arrivalTime(mDepartDateTime).alternatives(false).await();
                                }
                            }
                        }
                    } catch (Exception e) {
                        Toast.makeText(getActivity(), "Error searching, no direction found or make sure all fields are set.", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Long l) {
            if (!error) {
                progressDialog.dismiss();
                try {
                    for (int i = 0; i < mResult.routes[0].legs[0].steps.length; i++) {
                        if (mResult.routes[0].legs[0].steps[i].htmlInstructions.contains("Bus")) {
                            throw new Exception();
                        }
                    }
                } catch (Exception e) {
                    Toast.makeText(getActivity(), "No route found or supported, try a more specific place", Toast.LENGTH_SHORT).show();
                    return;
                }
                try {
                    ((MainActivity) getActivity()).launchDirectionResult(mResult, mOrigin.getText().toString(), mDestination.getText().toString(), mDepartArriveText.getTag().toString(), mDepartDateTime);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Error searching, make sure all fields are set.", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    //Load necessary data on start
    private class loadData extends AsyncTask<URL, Integer, Long> {

        ProgressDialog progressDialog = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            progressDialog.setTitle("Loading");
            progressDialog.setMessage("Please wait while the system loads your data from the cloud");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Long doInBackground(URL... urls) {
            try {
                mDepartArriveText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        timePicker();
                    }
                });
                mDepartArriveText.setEnabled(false);
                mStartDirection.setVisibility(View.INVISIBLE);


                mSpinner.setAdapter(mAdapter);

                mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        String selected = adapterView.getItemAtPosition(i).toString();
                        if (selected.equalsIgnoreCase("Now")) {
                            //mDepartArriveText.setText("");
                            mDepartArriveText.setTag("Now");
                            mDepartArriveText.setVisibility(View.INVISIBLE);
                        } else {
                            if (selected.equalsIgnoreCase("Set Departure Time")) {
                                mDepartArriveText.setTag("Depart");
                                mDepartArriveText.setVisibility(View.VISIBLE);
                            } else {
                                mDepartArriveText.setTag("Arrive");
                                mDepartArriveText.setVisibility(View.VISIBLE);
                            }
                        }
                        //Toast.makeText(getContext(), adapterView.getItemAtPosition(i).toString(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                mSpinner.setEnabled(false);

                mCurrentPin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mOrigin.isEnabled()){
                            if (mMarkerOrigin != null){
                                mMarkerOrigin.remove();
                            }
                        }
                        mOrigin.setText("Current Location");
                    }
                });
                mCurrentPin.setEnabled(false);

                mOrigin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        launchPlaceSearch(view, mOrigin.getTag().toString());
                    }
                });
                mOrigin.setEnabled(false);


                mDestination.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        launchPlaceSearch(view, mDestination.getTag().toString());
                    }
                });
                mDestination.setEnabled(false);


                mNavigationSearch.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mNavigationSearch.getText().toString().equalsIgnoreCase("Search")) {
                            new DirectionAPI().execute();
                        } else {
                            cancelSchedule();
                        }
                    }
                });

                mStartDirection.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (checkLocationPermission()) {
                            Intent intent = new Intent(getActivity(), LiveDirectionActivity.class);
                            Intent i = new Intent(getActivity(), TrackTrainService.class);
                            getActivity().stopService(i);
                            startActivity(intent);
                        }
                        else{
                            Toast.makeText(getContext(), "Please allow location permission to use this function!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });


                originRef = mDatabase.getReference("User").child(mAuth.auth.getCurrentUser().getUid()).child("ScheduleTrip");
                originRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        try {
                            String origin = dataSnapshot.child("origin").getValue(String.class);
                            Double lat = dataSnapshot.child("Trip").child("Waypoint_0").child("Start Location").child("lat").getValue(Double.class);
                            final LatLng latLng;
                            if (lat != null) {
                                latLng = new LatLng(dataSnapshot.child("Trip").child("Waypoint_0").child("Start Location").child("lat").getValue(Double.class), dataSnapshot.child("Trip").child("Waypoint_0").child("Start Location").child("lng").getValue(Double.class));
                            }
                            else {
                                latLng = null;
                            }
                            if (origin != null && latLng != null) {
                                geoDataClient.getPlaceById(origin).addOnCompleteListener(new OnCompleteListener<PlaceBufferResponse>() {
                                    @Override
                                    public void onComplete(@NonNull Task<PlaceBufferResponse> task) {
                                        PlaceBufferResponse places = task.getResult();
                                        Place origin = places.get(0);
                                        mOrigin.setText(origin.getName());
                                        if (mMarkerOrigin != null) {
                                            mMarkerOrigin.remove();
                                            mMarkerOrigin = mgoogleMap.addMarker(new MarkerOptions().position(latLng));
                                        } else {
                                            mMarkerOrigin = mgoogleMap.addMarker(new MarkerOptions().position(latLng));
                                        }
                                        places.release();
                                    }
                                });
                                mCurrentPin.setEnabled(false);
                                mOrigin.setEnabled(false);
                            } else {
                                mCurrentPin.setEnabled(true);
                                mOrigin.setEnabled(true);
                            }
                        }
                        catch (Exception e){

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                destinationRef = mDatabase.getReference("User").child(mAuth.auth.getCurrentUser().getUid()).child("ScheduleTrip").child("destination");
                destinationRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String destination = dataSnapshot.getValue(String.class);
                        if (destination != null) {

                            addMarker(destination);

                            mDestination.setEnabled(false);
                            mStartDirection.setVisibility(View.VISIBLE);
                            mNavigationSearch.setText("Cancel");
                        } else {
                            mDestination.setEnabled(true);
                            mStartDirection.setVisibility(View.INVISIBLE);
                            mNavigationSearch.setText("Search");
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                departTypeRef = mDatabase.getReference("User").child(mAuth.auth.getCurrentUser().getUid()).child("ScheduleTrip").child("departType");
                departTypeRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String departType = dataSnapshot.getValue(String.class);
                        if (departType != null) {
                            if (departType.equalsIgnoreCase("Now")) {
                                mSpinner.setSelection(0);
                            } else if (departType.equalsIgnoreCase("Depart")) {
                                mSpinner.setSelection(1);
                            } else {
                                mSpinner.setSelection(2);
                            }
                            mSpinner.setEnabled(false);
                        } else {
                            mSpinner.setEnabled(true);
                            mDepartArriveText.setEnabled(true);
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                departTimeRef = mDatabase.getReference("User").child(mAuth.auth.getCurrentUser().getUid()).child("ScheduleTrip").child("departTime");
                departTimeRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String departTimeRef = dataSnapshot.getValue(String.class);
                        if (departTimeRef != null && !departTimeRef.equalsIgnoreCase("")) {
                            Instant instant = new Instant(departTimeRef);
                            DateTime dateTime = instant.toDateTime();
                            String time = dateTime.getHourOfDay() + ":" + dateTime.getMinuteOfHour();
                            mDepartArriveText.setText(time);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                encodedPathRef = mDatabase.getReference("User").child(mAuth.auth.getCurrentUser().getUid()).child("ScheduleTrip").child("encodedPath");
                encodedPathRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        try {
                            String encodedPathRef = dataSnapshot.getValue(String.class);
                            if (encodedPathRef != null && !encodedPathRef.equalsIgnoreCase("")) {
                                List<LatLng> decodedPath = PolyUtil.decode(encodedPathRef);
                                mDirectionLine = mgoogleMap.addPolyline(new PolylineOptions().addAll(decodedPath));

                            }
                            progressDialog.dismiss();
                        }
                        catch (Exception e){

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Long l) {
        }
    }
}

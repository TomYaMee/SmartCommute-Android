package com.tomyamee.smartcommute;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class HelpActivity extends AppCompatActivity {

    private ViewPager mViewPager;
    private ViewPagerAdapater mViewPagerAdapter;
    private LinearLayout dots;
    private TextView[] d0tsText;
    private int[] layouts;
    private Button btnBack, btnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        mViewPager = findViewById(R.id.helpViewPager);
        dots = findViewById(R.id.helpDots);
        btnBack = findViewById(R.id.helpButtonBack);
        btnNext = findViewById(R.id.helpButtonNext);

        layouts = new int[]{R.layout.help_slide1, R.layout.help_slide2, R.layout.help_slide3, R.layout.help_slide4, R.layout.help_slide5};

        System.out.println("yes");
        addBottomDots(0);

        mViewPagerAdapter = new ViewPagerAdapater();
        mViewPager.setAdapter(mViewPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                addBottomDots(position);

                if (position == layouts.length - 1){
                    btnNext.setText("Got It");
                }
                else{
                    btnNext.setText("Next");
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItem(+1);
                if (current < layouts.length){
                    mViewPager.setCurrentItem(current);
                }
                else{
                    finish();
                }
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItem(-1);
                if (current > -1){
                    mViewPager.setCurrentItem(current);
                }
                else{
                    finish();
                }
            }
        });
    }

    private int getItem(int i) {
        return mViewPager.getCurrentItem() + i;
    }

    private void addBottomDots(int i) {
        d0tsText = new TextView[layouts.length];

        int colorsActive = getResources().getColor(R.color.white);
        int colorsInactive = getResources().getColor(R.color.colorDarkGrey);

        dots.removeAllViews();
        for (int i2 = 0; i2 < d0tsText.length; i2 ++){
            d0tsText[i2] = new TextView(this);
            d0tsText[i2].setText(Html.fromHtml("&#8226"));
            d0tsText[i2].setTextSize(35);
            d0tsText[i2].setTextColor(colorsInactive);
            dots.addView(d0tsText[i2]);
        }

        if (d0tsText.length > 0){
            d0tsText[i].setTextColor(colorsActive);
        }
    }

    private class ViewPagerAdapater extends PagerAdapter {
        private LayoutInflater layoutInflater;

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}

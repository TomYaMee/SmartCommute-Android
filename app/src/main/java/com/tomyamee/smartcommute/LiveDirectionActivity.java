package com.tomyamee.smartcommute;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBufferResponse;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.android.PolyUtil;
import com.lsjwzh.widget.recyclerviewpager.*;

import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class LiveDirectionActivity extends AppCompatActivity {

    //Layout
    private MapView mMapView;
    private TextView mSpeed;

    //Google Map
    private GeoDataClient geoDataClient;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private LocationRequest mLocationRequest;
    private GoogleMap mgoogleMap;

    private CircleOptions mCircle;

    private LatLng defaultLocation; //To pinpoint location on map create
    private Intent intent; //To start foreground service
    private LocationManager locationManager;
    private LocationListener locationListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_direction);

        mMapView = findViewById(R.id.mapLiveView);
        mSpeed = findViewById(R.id.textSpeedometer);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                for (Location location : locationResult.getLocations()) {
                    if (defaultLocation == null) {
                        defaultLocation = new LatLng(location.getLatitude(), location.getLongitude());
                        CameraPosition cameraPosition = new CameraPosition.Builder().target(defaultLocation).zoom(12).build();
                        mgoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    }
                }
            }
        };
        geoDataClient = Places.getGeoDataClient(this, null);

        mMapView.onCreate(savedInstanceState);
        mMapView.onResume(); // needed to get the map to display immediately
        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                mgoogleMap = mMap;

                // For showing a move to my location button
                if (checkLocationPermission()) {
                    mgoogleMap.setMyLocationEnabled(true);
                    startLocate();
                }
            }
        });

        final List<DirectionData> directionData = new ArrayList<>();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        FirebaseAuth auth = FirebaseAuth.getInstance();
        DatabaseReference tripDetail = database.getReference().child("User").child(auth.getCurrentUser().getUid()).child("ScheduleTrip").child("Trip");
        tripDetail.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
                    SpannableStringBuilder builder = new SpannableStringBuilder();
                    try {
                        if (dataSnapshot1.child("TravelMode").getValue().toString().equalsIgnoreCase("WALKING")) {
                            builder.append(" ", new ImageSpan(getApplicationContext(), R.drawable.ic_directions_walk_black_24dp), 0);
                        } else {
                            builder.append(" ", new ImageSpan(getApplicationContext(), R.drawable.ic_subway_black_24dp), 0);
                        }
                        DateTimeZone dateTimeZone = DateTimeZone.forID("Asia/Kuala_Lumpur");
                        DateTime departureTime = new DateTime(dataSnapshot1.child("Depature Time").getValue(), dateTimeZone);
                        DateTime arrivalTime = new DateTime(dataSnapshot1.child("Arrival Time").getValue(), dateTimeZone);
                        DirectionData directionData1;
                        if (dataSnapshot1.child("TravelMode").getValue().toString().equalsIgnoreCase("WALKING")) {
                            directionData1 = new DirectionData(builder, dataSnapshot1.child("Direction").getValue().toString(), dataSnapshot1.child("Distance").getValue().toString(), dataSnapshot1.child("Duration").getValue().toString(), departureTime, arrivalTime, new com.google.maps.model.LatLng((double) dataSnapshot1.child("Start Location").child("lat").getValue(), (double) dataSnapshot1.child("Start Location").child("lng").getValue()), new com.google.maps.model.LatLng((double) dataSnapshot1.child("End Location").child("lat").getValue(), (double) dataSnapshot1.child("End Location").child("lng").getValue()), "");
                        }
                        else {
                            directionData1 = new DirectionData(builder, dataSnapshot1.child("Direction").getValue().toString(), dataSnapshot1.child("Distance").getValue().toString(), dataSnapshot1.child("Duration").getValue().toString(), departureTime, arrivalTime, new com.google.maps.model.LatLng((double) dataSnapshot1.child("Start Location").child("lat").getValue(), (double) dataSnapshot1.child("Start Location").child("lng").getValue()), new com.google.maps.model.LatLng((double) dataSnapshot1.child("End Location").child("lat").getValue(), (double) dataSnapshot1.child("End Location").child("lng").getValue()), dataSnapshot1.child("Departure Stop").getValue().toString());
                        }

                        directionData.add(directionData1);
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                }
                populateDirection(directionData);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        DatabaseReference encodedPathRef;
        encodedPathRef = database.getReference("User").child(auth.getCurrentUser().getUid()).child("ScheduleTrip").child("encodedPath");
        encodedPathRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String encodedPathRef = dataSnapshot.getValue(String.class);
                if (encodedPathRef != null && !encodedPathRef.equalsIgnoreCase("")) {
                    List<LatLng> decodedPath = PolyUtil.decode(encodedPathRef);
                    mgoogleMap.addPolyline(new PolylineOptions().addAll(decodedPath));

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        DatabaseReference originRef;
        originRef = database.getReference("User").child(auth.getCurrentUser().getUid()).child("ScheduleTrip").child("origin");
        originRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String origin = dataSnapshot.getValue(String.class);
                geoDataClient.getPlaceById(origin).addOnCompleteListener(new OnCompleteListener<PlaceBufferResponse>() {
                    @Override
                    public void onComplete(@NonNull Task<PlaceBufferResponse> task) {
                        PlaceBufferResponse places = task.getResult();
                        Place origin = places.get(0);
                        mgoogleMap.addMarker(new MarkerOptions().position(origin.getLatLng()));

                        places.release();
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        DatabaseReference destinationRef;
        destinationRef = database.getReference("User").child(auth.getCurrentUser().getUid()).child("ScheduleTrip").child("destination");
        destinationRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String destination = dataSnapshot.getValue(String.class);

                    geoDataClient.getPlaceById(destination).addOnCompleteListener(new OnCompleteListener<PlaceBufferResponse>() {
                        @Override
                        public void onComplete(@NonNull Task<PlaceBufferResponse> task) {
                            PlaceBufferResponse places = task.getResult();
                            Place destination = places.get(0);
                            mgoogleMap.addMarker(new MarkerOptions().position(destination.getLatLng()));

                            mCircle = new CircleOptions();
                            mCircle.center(destination.getLatLng());
                            mCircle.radius(25);
                            places.release();
                        }
                    });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                float speed = location.getSpeed();
                if (speed != 0) {
                    speed = ((speed * 60) * 60) / 1000;
                    DecimalFormat df = new DecimalFormat("0.00");
                    mSpeed.setText(df.format(speed) + "\nKM/H");
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

        if (checkLocationPermission()) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100, 0, locationListener);
        }

        intent = new Intent(this, TrackSpeedService.class);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            startForegroundService(intent);
        }
        else{
            startService(intent);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        endTrip();
    }

    @Override
    public void onResume(){
        super.onResume();
        mMapView.onResume();
        if (checkLocationPermission()) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100, 0, locationListener);
        }
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (checkLocationPermission()) {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        mMapView.onPause();
        locationManager.removeUpdates(locationListener);
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    private boolean checkLocationPermission() {
        return ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    //Immediately pinpoint device location on map ready
    private void startLocate() {
        if (checkLocationPermission())
            mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null) {
                        if (defaultLocation == null) {
                            defaultLocation = new LatLng(location.getLatitude(), location.getLongitude());
                            CameraPosition cameraPosition = new CameraPosition.Builder().target(defaultLocation).zoom(12).build();
                            mgoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }
                    }
                }
            });
    }

    private void populateDirection(List<DirectionData> directionData){
        RecyclerViewPager recyclerView = findViewById(R.id.recyclerViewLive);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        RecyclerView.Adapter adapter = new RecyclerAdapter(directionData);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new LinePagerIndicator());
    }

    private void endTrip(){
        mMapView.onPause();
        final float[] distance = new float[2];
        if (checkLocationPermission()) {
            mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    Location.distanceBetween(location.getLatitude(), location.getLongitude(), mCircle.getCenter().latitude, mCircle.getCenter().longitude, distance);
                    System.out.println(distance[0]);
                    if (distance[0] < 200){
                        final FirebaseDatabase database = FirebaseDatabase.getInstance();
                        final FirebaseAuth auth = FirebaseAuth.getInstance();
                        final DatabaseReference ref = database.getReference().child("User").child(auth.getCurrentUser().getUid());
                        ref.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                try {
                                    String distance = dataSnapshot.child("ScheduleTrip").child("distance").getValue().toString();
                                    Double distanceDouble = Double.valueOf(distance);
                                    distanceDouble = (distanceDouble / 1000);
                                    DecimalFormat df = new DecimalFormat("0.00");
                                    try {
                                        String distanceTravelled = dataSnapshot.child("Distance Travelled").getValue().toString();
                                        Double distanceTravalledDouble = Double.valueOf(distanceTravelled);
                                        distanceTravalledDouble = distanceTravalledDouble + distanceDouble;
                                        System.out.println(distanceTravelled);
                                        System.out.println(distanceTravalledDouble);
                                        ref.child("Distance Travelled").setValue(df.format(distanceTravalledDouble));
                                    } catch (Exception e) {
                                        ref.child("Distance Travelled").setValue(df.format(distanceDouble));
                                    } finally {
                                        locationManager.removeUpdates(locationListener);
                                        mFusedLocationClient.removeLocationUpdates(mLocationCallback);


                                        stopService(intent);
                                        finish();
                                    }
                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                    else if (distance[0] > 200){
                        locationManager.removeUpdates(locationListener);
                        mFusedLocationClient.removeLocationUpdates(mLocationCallback);


                        Intent i = new Intent(getApplicationContext(), TrackTrainService.class);
                        stopService(i);
                        stopService(intent);
                        finish();
                    }
                }
            });
        }
    }

}

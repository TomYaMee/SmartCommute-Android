package com.tomyamee.smartcommute;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.maps.model.DirectionsResult;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Instant;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView navigation;
    private Authentication auth;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            //http://www.truiton.com/2017/01/android-bottom-navigation-bar-example/
            switch (item.getItemId()) {
                case R.id.navigation_schedule:
                    changeFragment("Schedule");
                    return true;
                //break;
                case R.id.navigation_live:
                    changeFragment("Live");
                    return true;
                //break;
                case R.id.navigation_account:
                    changeFragment("Account");
                    return true;
                //break;
            }
            return false;
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.help, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        System.out.println(item.getItemId());
        if (item.getItemId() == R.id.helpButton){
            Intent i = new Intent(this, HelpActivity.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        Fragment selectedFragment = MapFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, selectedFragment);
        transaction.commit();

        auth = new Authentication(this);
    }

    //Switch content frame based on navigation button
    public void changeFragment(String fragment) {
        Fragment selectedFragment = MapFragment.newInstance();
        switch (fragment) {
            case "Schedule":
                selectedFragment = MapFragment.newInstance();
                //return true;
                break;
            case "Live":
                selectedFragment = LiveFragment.newInstance();
                //return true;
                break;
            case "Account":
                selectedFragment = AccountFragment.newInstance();
                //return true;
                break;
            case "Error":
                selectedFragment = ErrorFragment.newInstance();
                break;
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, selectedFragment);
        transaction.commit();
    }

    public void launchDirectionResult(DirectionsResult dr, String origin, String destination, String departureType, Instant time) {
        Direction direction = new Direction(dr, origin, destination, departureType, time);
        final Calendar c = Calendar.getInstance();
        final int hour = c.get(Calendar.HOUR_OF_DAY);
        final int minute = c.get(Calendar.MINUTE);
        final int year = c.get(Calendar.YEAR);
        final int month = c.get(Calendar.MONTH);
        final int day = c.get(Calendar.DAY_OF_MONTH);
        DateTime dateTime = new DateTime(year, month + 1, day, hour, minute, DateTimeZone.forID("Asia/Kuala_Lumpur"));
        try {
            if (dr.routes[0].legs[0].departureTime.isBefore(dateTime)) {
                Toast.makeText(this, "No route found \nDeparture time is way earlier, choose a different Arrival time", Toast.LENGTH_SHORT).show();
            } else if (dr.routes.length > 0) {
                Intent intent = new Intent(this, ActivityDirection.class);
                intent.putExtra("directionResult", direction);
                startActivity(intent);
            } else {
                Toast.makeText(this, "No route found", Toast.LENGTH_SHORT).show();
            }
        }
        catch(Exception e){
            if (dr.routes.length > 0) {
                Intent intent = new Intent(this, ActivityDirection.class);
                intent.putExtra("directionResult", direction);
                startActivity(intent);
            } else {
                Toast.makeText(this, "No route found", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void startSignOut() {
        System.out.println("[Auth] Start sign out");
        auth.signOut();

        Intent intent = new Intent(this, LandingActivity.class);
        startActivity(intent);
        finish();
    }


    @Override
    public void onDestroy() {

        super.onDestroy();
        System.out.println("[Auth] Destroy");
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void cancelJob(){
        Intent i = new Intent(getApplicationContext(), TrackTrainService.class);
        stopService(i);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}

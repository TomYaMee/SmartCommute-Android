package com.tomyamee.smartcommute;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by tommy on 01/08/2018.
 */

//Used to add data to list using custom UI
//Reference: https://stackoverflow.com/questions/8166497/custom-adapter-for-list-view
@SuppressWarnings("DefaultFileTemplate")
public class DirectionAdapter extends ArrayAdapter<DirectionData> {

    private int mLayoutResource;

    public DirectionAdapter(Context context, int layoutReSource, List<DirectionData> directionDataList) {
        super(context, layoutReSource, directionDataList);
        this.mLayoutResource = layoutReSource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            view = layoutInflater.inflate(mLayoutResource, null);
        }

        DirectionData dd = getItem(position);

        if (dd != null) {
            final TextView textIcon = view.findViewById(R.id.textLiveIcon);
            final TextView textDirection = view.findViewById(R.id.textLiveDirection);
            final TextView textDistance = view.findViewById(R.id.textLiveDistance);
            final TextView textDuration = view.findViewById(R.id.textLiveDuration);

            if (textIcon != null) {
                textIcon.setText(dd.getmIcon());
            }
            if (textDirection != null) {
                textDirection.setText(dd.getmDirection());
            }
            if (textDistance != null) {
                textDistance.setText(dd.getmDistance());
            }
            if (textDuration != null) {
                String departure;
                String arrival;
                if (dd.getmDepartureTime().getMinuteOfHour() < 10){
                    departure = dd.getmDepartureTime().getHourOfDay() + ":0" + dd.getmDepartureTime().getMinuteOfHour();
                }
                else{
                    departure = dd.getmDepartureTime().getHourOfDay() + ":" + dd.getmDepartureTime().getMinuteOfHour();
                }
                if (dd.getmArrivalTime().getMinuteOfHour() < 10){
                    arrival = dd.getmArrivalTime().getHourOfDay() + ":0" + dd.getmArrivalTime().getMinuteOfHour();
                }
                else{
                    arrival = dd.getmArrivalTime().getHourOfDay() + ":" + dd.getmArrivalTime().getMinuteOfHour();
                }
                textDuration.setText(dd.getmDuration() + " (" + departure + " - " + arrival + ")");
            }
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }


        return view;
    }

}

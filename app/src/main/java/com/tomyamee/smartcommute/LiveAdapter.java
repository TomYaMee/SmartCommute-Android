package com.tomyamee.smartcommute;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by tommy on 01/08/2018.
 */

//Used to add data to list using custom UI
//Reference: https://stackoverflow.com/questions/8166497/custom-adapter-for-list-view
@SuppressWarnings("DefaultFileTemplate")
public class LiveAdapter extends ArrayAdapter<LiveData> {

    private int layoutResource;

    public LiveAdapter(Context context, int layoutReSource, List<LiveData> liveDataList) {
        super(context, layoutReSource, liveDataList);
        this.layoutResource = layoutReSource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            view = layoutInflater.inflate(layoutResource, null);
        }

        LiveData ld = getItem(position);

        if (ld != null) {
            final TextView origin = view.findViewById(R.id.liveDestination);
            final TextView destination = view.findViewById(R.id.liveTime);
            TextView current = view.findViewById(R.id.liveCurrent);
            TextView status = view.findViewById(R.id.liveStatus);

            if (origin != null) {
                origin.setText(ld.getDirection());
            }
            if (destination != null) {
                String depart;
                if (ld.getDepartureTime().getMinuteOfHour() < 10){
                    depart = ld.getDepartureTime().getHourOfDay() + ":0" + ld.getDepartureTime().getMinuteOfHour();
                }
                else{
                    depart = ld.getDepartureTime().getHourOfDay() + ":" + ld.getDepartureTime().getMinuteOfHour();
                }
                destination.setText(depart);
            }
            if (current != null) {
                current.setText(ld.getLocation());
            }
            if (status != null) {
                status.setText(ld.getStatus());
            }
        }


        return view;
    }

}

package com.tomyamee.smartcommute;

import com.google.maps.model.DirectionsResult;

import org.joda.time.Instant;

import java.io.Serializable;

/**
 * Created by tommy on 01/21/2018.
 */

//Implement serializable for data transfer between activity
@SuppressWarnings("DefaultFileTemplate")
public class Direction implements Serializable {

    private DirectionsResult mDirectionsResult;
    private String mOrigin;
    private String mDestination;
    private String mDepartureType;
    private Instant mDepartTime;

    public Direction(DirectionsResult dr, String origin, String destination, String departureType, Instant time) {
        this.mDirectionsResult = dr;
        this.mOrigin = origin;
        this.mDestination = destination;
        this.mDepartureType = departureType;
        this.mDepartTime = time;
    }

    public DirectionsResult getmDirectionsResult() {
        return mDirectionsResult;
    }

    public String getmDepartureType() {
        return mDepartureType;
    }

    public String getmDestination() {
        return mDestination;
    }

    public String getmOrigin() {
        return mOrigin;
    }

    public Instant getmDepartTime() {
        return mDepartTime;
    }
}

package com.tomyamee.smartcommute;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by tommy on 01/22/2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class Authentication {

    FirebaseAuth auth;
    AppCompatActivity mActivity;

    public Authentication(AppCompatActivity activity) {
        auth = FirebaseAuth.getInstance();
        this.mActivity = activity;
    }


    public void signIn() {
        auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null) {
            Intent intent = new Intent(mActivity, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            mActivity.startActivity(intent);
        } else {
            ((LandingActivity) mActivity).startSignIn();
        }
    }

    public void signOut() {
        auth.signOut();
        Toast.makeText(mActivity, "Signed out successfully", Toast.LENGTH_SHORT).show();
    }
}

package com.tomyamee.smartcommute;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBufferResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.AddressType;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Instant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LiveFragment extends Fragment {

    private List<LiveData> ldlist = new ArrayList<>();
    private ListView list;
    private LiveAdapter la;

    private GeoDataClient mGeoDataClient;

    public static LiveFragment newInstance() {
        return new LiveFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_live, container, false);

        list = view.findViewById(R.id.livelist);

        mGeoDataClient = Places.getGeoDataClient(getActivity(), null);

        la = new LiveAdapter(getActivity(), R.layout.live_list, ldlist);
        list.setAdapter(la);

        final FirebaseAuth auth = FirebaseAuth.getInstance();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        DatabaseReference tripDetail = database.getReference().child("User").child(auth.getCurrentUser().getUid()).child("ScheduleTrip").child("Trip");
        tripDetail.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    String direction;
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        direction = dataSnapshot1.child("Direction").getValue().toString();
                        if (direction.contains("Train towards") || direction.contains("Subway towards") || direction.contains("Monorail towards")) {
                            final String subwayLine;
                            if (direction.contains("Subway towards")) {
                                subwayLine = dataSnapshot1.child("Subway Line").getValue().toString();
                            }
                            else{
                                subwayLine = null;
                            }
                            Instant departure = new Instant(dataSnapshot1.child("Departure Time").getValue().toString());
                            DateTimeZone dateTimeZone = DateTimeZone.forID("Asia/Kuala_Lumpur");
                            final DateTime departureTIme = new DateTime(departure, dateTimeZone);
                            /*GeoApiContext context = new GeoApiContext.Builder().apiKey("AIzaSyBucrA-XR6nEOQ-AMmG3pObvR7oR3eEcYs").build();
                            Double lat = Double.valueOf(dataSnapshot1.child("Start Location").child("lat").getValue().toString());
                            Double lng = Double.valueOf(dataSnapshot1.child("Start Location").child("lng").getValue().toString());
                            LatLng latLng = new LatLng(lat, lng);
                            GeocodingResult[] result;
                            try {
                                result = GeocodingApi.newRequest(context).latlng(latLng).resultType(AddressType.TRANSIT_STATION).await();
                                final String finalDirection = direction;
                                mGeoDataClient.getPlaceById(result[0].placeId).addOnCompleteListener(new OnCompleteListener<PlaceBufferResponse>() {
                                    @Override
                                    public void onComplete(@NonNull Task<PlaceBufferResponse> task) {
                                        if (task.isSuccessful()) {
                                            PlaceBufferResponse places = task.getResult();
                                            Place myPlace = places.get(0);
                                            String startLocation = myPlace.getName().toString();

                                            places.release();
                                        }
                                    }
                                });
                            } catch (ApiException e) {
                                e.printStackTrace();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }*/
                            String depatureStation = dataSnapshot1.child("Departure Stop").getValue().toString();
                            if (depatureStation.toLowerCase().contains(" Station")){
                                depatureStation.replace(" Station", "");
                            }
                            getStatus(direction, departureTIme, depatureStation, subwayLine);
                        }
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                    System.out.println("Error 1");
                    ((MainActivity)getActivity()).changeFragment("Error");
                    return;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        DatabaseReference tripCheck = database.getReference().child("User").child(auth.getCurrentUser().getUid());
        tripCheck.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    if (dataSnapshot.child("ScheduleTrip").getValue() == null) {
                        ((MainActivity) getActivity()).changeFragment("Error");
                        return;
                    }
                }
                catch(Exception e){

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return view;
    }

    private void getStatus(final String direction, final DateTime departureTIme, final String startLocation, String subwayLine) {
        if (direction.contains("Train towards") || direction.contains("Subway towards") || direction.contains("Monorail towards")) {
            String station;
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference stationName;
            if (direction.contains("Train towards")){
                station = direction.substring(14);
                stationName = database.getReference().child("Train").child("KTM").child(station);
            }
            else if (direction.contains("Subway towards to")){
                station = direction.substring(18);
                if (station.contains("Putra Heights")) {
                    if (subwayLine.equalsIgnoreCase("SPL")) {
                        stationName = database.getReference().child("Train").child("LRT").child("SPL-Putra Heights");
                    }
                    else{
                        stationName = database.getReference().child("Train").child("LRT").child("KJL-Putra Heights");
                    }
                }
                else if (station.contains("Sentul Timur")){
                    if (subwayLine.equalsIgnoreCase("AGL")){
                        stationName = database.getReference().child("Train").child("LRT").child("AGL-Sentul Timur");
                    }
                    else{
                        stationName = database.getReference().child("Train").child("LRT").child(station);
                    }
                }
                else{
                    stationName = database.getReference().child("Train").child("LRT").child(station);
                }
            }
            else{
                station = direction.substring(20);
                stationName = database.getReference().child("Train").child("Monorail").child(station);
            }
            /*
            if (station.contains("/")){
                station = station.replace("/", " ");
            }*/
            System.out.println(stationName.getKey());
            stationName.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    int departureStationCount = 0;
                    List<Integer> duration = new ArrayList<>();
                    List<DateTime> departureTimeList = new ArrayList<>();
                    List<String> stationNameList = new ArrayList<>();
                    //Get list of stattion name"
                    for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
                        if (!dataSnapshot1.getKey().equalsIgnoreCase("Status")){
                            duration.add(Integer.valueOf(dataSnapshot1.child("Duration").getValue().toString()));
                            stationNameList.add(dataSnapshot1.child("Station Name").getValue().toString());
                        }
                    }

                    //Find number of station before reaching the station needed
                    for (String list: stationNameList){
                        if (!list.equalsIgnoreCase(startLocation)){
                            System.out.println(startLocation + " >> " + list);
                            departureStationCount++;
                        }
                        else{
                            break;
                        }
                    }

                    System.out.println(departureStationCount);
                    //Find departure time one station before until starting location
                    int loopCount = 0;
                    for (int i = departureStationCount; i > 0; i--){
                        if (i == departureStationCount){
                            departureTimeList.add(departureTIme.minusMinutes(duration.get(i)));
                            loopCount++;
                        }
                        else{
                            departureTimeList.add(departureTimeList.get(loopCount - 1).minusMinutes(duration.get(i)));
                            loopCount++;
                        }

                    }

                    //Reverse so the data starts from starting location
                    Collections.reverse(departureTimeList);

                    DateTimeZone dateTimeZone = DateTimeZone.forID("Asia/Kuala_Lumpur");
                    DateTime currentDateTime = new DateTime(dateTimeZone);

                    //Check data with current time
                    try {
                        String status = dataSnapshot.child("Status").getValue().toString();
                        if (currentDateTime.isBefore(departureTimeList.get(0))) {
                            addToList(direction, departureTIme, "Not operational", "Train not running yet");
                        } else {
                            for (int i = 0; i < departureTimeList.size(); i++) {
                                if (i == (departureTimeList.size() - 1) && currentDateTime.isAfter(departureTimeList.get(i))) {
                                    addToList(direction, departureTIme, status, ("Enrouting to " + stationNameList.get(i + 1)));
                                    break;
                                } else if (currentDateTime.isAfter(departureTimeList.get(i)) && currentDateTime.isBefore(departureTimeList.get(i + 1))) {
                                    addToList(direction, departureTIme, status, ("Enrouting to " + stationNameList.get(i + 1)));
                                    break;
                                } else if (currentDateTime.isAfter(departureTIme)) {
                                    addToList(direction, departureTIme, status, ("Departed"));
                                    break;
                                }
                            }
                        }
                    }
                    catch(Exception e){
                        System.out.println("Error 2");
                        e.printStackTrace();
                        ((MainActivity)getActivity()).changeFragment("Error");
                        return;
                    }



                    setAdapter(ldlist);

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    private void addToList(String direction, DateTime departureTime, String status, String startLocation){
        LiveData ld = new LiveData(direction, departureTime, status, startLocation);
        ldlist.add(ld);
    }


    private void setAdapter(List<LiveData> ldlist){

        if (ldlist.size() == 0){
            System.out.println("Error 3");
            ((MainActivity)getActivity()).changeFragment("Error");
            return;
        }
        else {
            la.notifyDataSetChanged();
        }
    }
}

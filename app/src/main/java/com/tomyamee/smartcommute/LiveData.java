package com.tomyamee.smartcommute;

import org.joda.time.DateTime;

/**
 * Created by tommy on 01/13/2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class LiveData {

    private String direction;
    private DateTime departureTime;
    private String status;
    private String location;

    public LiveData(String direction, DateTime departureTime, String status, String location) {

        this.direction = direction;
        this.departureTime = departureTime;
        this.status = status;
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public String getDirection() {
        return direction;
    }

    public DateTime getDepartureTime() {
        return departureTime;
    }

    public String getLocation() {
        return location;
    }
}

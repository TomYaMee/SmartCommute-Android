package com.tomyamee.smartcommute;

import android.text.SpannableStringBuilder;

import com.google.maps.model.LatLng;

import org.joda.time.DateTime;

/**
 * Created by tommy on 01/08/2018.
 */

//Used to format data using Custom Adapter

@SuppressWarnings("DefaultFileTemplate")
public class DirectionData {

    private SpannableStringBuilder mIcon;
    private String mDirection;
    private String mDistance;
    private String mDuration;
    private DateTime mDepartureTime;
    private DateTime mArrivalTime;
    private String mDepatureStop;
    private LatLng mStartLocation;
    private LatLng mEndLocation;

    public DirectionData(SpannableStringBuilder icon, String direction, String distance, String duration, DateTime departureTime, DateTime arrivalTime, LatLng startLocation, LatLng endLocation, String depatureStop) {
        this.mIcon = icon;
        this.mDirection = direction;
        this.mDistance = distance;
        this.mDuration = duration;
        this.mDepartureTime = departureTime;
        this.mArrivalTime = arrivalTime;
        this.mStartLocation = startLocation;
        this.mEndLocation = endLocation;
        this.mDepatureStop = depatureStop;
    }

    public SpannableStringBuilder getmIcon() {
        return mIcon;
    }

    public String getmDirection() {
        return mDirection;
    }

    public String getmDistance() {
        return mDistance;
    }

    public String getmDuration() {
        return mDuration;
    }

    public DateTime getmDepartureTime() {
        return mDepartureTime;
    }

    public DateTime getmArrivalTime() {
        return mArrivalTime;
    }

    public LatLng getmStartLocation() {
        return mStartLocation;
    }

    public LatLng getmEndLocation() {
        return mEndLocation;
    }

    public String getmDepatureStop() {
        return mDepatureStop;
    }
}

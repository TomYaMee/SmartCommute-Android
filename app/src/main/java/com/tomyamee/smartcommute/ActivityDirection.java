package com.tomyamee.smartcommute;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.GeocodingApiRequest;
import com.google.maps.model.AddressType;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsStep;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import com.google.maps.model.TransitMode;
import com.google.maps.model.TravelMode;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Instant;
import org.joda.time.Minutes;
import org.joda.time.Period;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ActivityDirection extends AppCompatActivity {

    private DirectionsResult mDirectionsResult;
    private SpannableStringBuilder mDirectionOverview;
    private String mDirection;
    private String mDuration;
    private String mDistance;
    private ArrayList<LocalDirection> mLocalDirection;
    private List<DirectionData> mDDList;
    private float mAvgSpeed;
    private Minutes mMinutes;

    //UI Layout
    private ListView mList;
    private Direction mDirectionUI;
    private TextView mTextDirectionOverview;
    private TextView mTextDuration;
    private TextView mTotalDuration;
    private Button mButtonSchedule;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direction);

        mTextDirectionOverview = findViewById(R.id.textLiveDirection);
        mTextDuration = findViewById(R.id.textLiveDuration);
        mTotalDuration = findViewById(R.id.textTotalTime);
        mButtonSchedule = findViewById(R.id.buttonSchedule);

        mButtonSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scheduleTrip();
                finish();
            }
        });

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        FirebaseAuth auth = FirebaseAuth.getInstance();
        database.getReference().child("User").child(auth.getCurrentUser().getUid()).child("AverageSpeed").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null){
                    mAvgSpeed = 0;
                }
                else {
                    mAvgSpeed = Float.valueOf(dataSnapshot.getValue().toString());
                }
                new CustomDirectionAPI().execute();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void getDirection() {
        Intent intent = getIntent();
        mDirectionUI = (Direction) intent.getSerializableExtra("directionResult");
        mDirectionsResult = mDirectionUI.getmDirectionsResult();
        mDDList = new ArrayList<>();

        mLocalDirection = new ArrayList<>();
        mDirectionOverview = new SpannableStringBuilder();
        DirectionData dd;
        Period period = new Period();

        //Draw arrows for mDirection overview
        for (int i = 0; i < mDirectionsResult.routes[0].legs[0].steps.length; i++) {
            DirectionsStep directionsStep = mDirectionsResult.routes[0].legs[0].steps[i];
            if (directionsStep.travelMode.toString().equalsIgnoreCase("WALKING")) {
                mDirectionOverview.append(" ", new ImageSpan(this, R.drawable.ic_directions_walk_black_24dp), 0);
                if (i + 1 != mDirectionsResult.routes[0].legs[0].steps.length) {
                    mDirectionOverview.append(" ", new ImageSpan(this, R.drawable.ic_keyboard_arrow_right_black_24dp), 0);
                }
            } else {
                mDirectionOverview.append(" ", new ImageSpan(this, R.drawable.ic_subway_black_24dp), 0);
                if (i + 1 != mDirectionsResult.routes[0].legs[0].steps.length) {
                    mDirectionOverview.append(" ", new ImageSpan(this, R.drawable.ic_keyboard_arrow_right_black_24dp), 0);
                }
            }
        }

        //Reverse direction query
        if (mDirectionUI.getmDepartureType().equalsIgnoreCase("Arrive")){
            try {
                int loopCount = 0;
                for (int i = mDirectionsResult.routes[0].legs[0].steps.length; i > 0; i--) {
                    DirectionsStep directionsStep = mDirectionsResult.routes[0].legs[0].steps[i - 1];
                    mDirection = directionsStep.htmlInstructions;
                    mDuration = directionsStep.duration.toString();
                    mDistance = directionsStep.distance.toString();

                    DateTime departure;
                    DateTime arrival;

                    int durationInSecond;

                    if (i == mDirectionsResult.routes[0].legs[0].steps.length) {
                        if (mDirectionsResult.routes[0].legs[0].arrivalTime != null) {
                            arrival = mDirectionsResult.routes[0].legs[0].arrivalTime;
                        } else {
                            arrival = new DateTime(mDirectionUI.getmDepartTime(), DateTimeZone.forID("Asia/Kuala_Lumpur"));
                        }

                        if (directionsStep.travelMode.toString().equalsIgnoreCase("WALKING") && mAvgSpeed > 0) {
                            double distance = (double) directionsStep.distance.inMeters;
                            distance = distance / 1000;
                            durationInSecond = (int) ((distance / (mAvgSpeed / 60)) * 60);
                        } else {
                            durationInSecond = (int) directionsStep.duration.inSeconds;
                        }
                        departure = arrival.minusSeconds(durationInSecond);
                        if (mDirectionsResult.routes[0].legs[0].steps.length == 1) {
                            period = new Period(departure, arrival);
                        }
                        LocalDirection ld = new LocalDirection(mDirection, mDuration, mDistance, departure, arrival, directionsStep.startLocation, directionsStep.endLocation, "");
                        mLocalDirection.add(ld);
                    } else {
                        arrival = mLocalDirection.get(loopCount - 1).departureTime;
                        System.out.println("Previous " + arrival);
                        System.out.println("Start: " + directionsStep.startLocation);
                        System.out.println("End:" + directionsStep.endLocation);
                        if (mDirectionsResult.routes[0].legs[0].steps[i].travelMode.toString().equalsIgnoreCase("WALKING")) {
                            DirectionsResult customDirectionsResult;
                            customDirectionsResult = reverseCustomDirection(mLocalDirection.get(loopCount - 1).departureTime, directionsStep.startLocation, directionsStep.endLocation);
                            DirectionsStep customDirectionsStep = customDirectionsResult.routes[0].legs[0].steps[0];
                            for (int i2 = 0; i2 < customDirectionsResult.routes[0].legs[0].steps.length; i++){
                                if (!customDirectionsResult.routes[0].legs[0].steps[i2].travelMode.toString().equalsIgnoreCase("WALKING")){
                                    customDirectionsStep = customDirectionsResult.routes[0].legs[0].steps[i2];
                                    break;
                                }
                            }
                            arrival = customDirectionsResult.routes[0].legs[0].arrivalTime;
                            System.out.println("After " + arrival);
                            durationInSecond = (int) customDirectionsStep.duration.inSeconds;
                            departure = arrival.minusSeconds(durationInSecond);
                            LocalDirection ld = new LocalDirection(mDirection, mDuration, mDistance, departure, arrival, customDirectionsStep.startLocation, customDirectionsStep.endLocation, customDirectionsStep.transitDetails.departureStop.name);
                            mLocalDirection.add(ld);

                        } else {
                            if (directionsStep.travelMode.toString().equalsIgnoreCase("WALKING") && mAvgSpeed > 0) {
                                double distance = (double) directionsStep.distance.inMeters;
                                distance = distance / 1000;
                                durationInSecond = (int) ((distance / (mAvgSpeed / 60)) * 60);
                            } else {
                                durationInSecond = (int) directionsStep.duration.inSeconds;
                            }
                            departure = arrival.minusSeconds(durationInSecond);
                            LocalDirection ld = new LocalDirection(mDirection, mDuration, mDistance, departure, arrival, directionsStep.startLocation, directionsStep.endLocation, "");
                            mLocalDirection.add(ld);
                        }
                    }
                    SpannableStringBuilder builder = new SpannableStringBuilder();
                    if (directionsStep.travelMode.toString().equalsIgnoreCase("WALKING")) {
                        builder.append(" ", new ImageSpan(this, R.drawable.ic_directions_walk_black_24dp), 0);
                    } else {
                        builder.append(" ", new ImageSpan(this, R.drawable.ic_subway_black_24dp), 0);
                        if (directionsStep.transitDetails.numStops == 1) {
                            mDistance = mDistance + " (" + directionsStep.transitDetails.arrivalStop.name + " - " + directionsStep.transitDetails.numStops + " Stop)";
                        } else {
                            mDistance = mDistance + " (" + directionsStep.transitDetails.arrivalStop.name + " - " + directionsStep.transitDetails.numStops + " Stops)";
                        }
                    }

                    dd = new DirectionData(builder, mDirection, mDistance, ((int) Math.ceil(durationInSecond / 60) + " Mins"), mLocalDirection.get(loopCount).departureTime, mLocalDirection.get(loopCount).arrivalTime, mLocalDirection.get(loopCount).startLocation, mLocalDirection.get(loopCount).endLocation, mLocalDirection.get(loopCount).departureStation);
                    mDDList.add(dd);
                    loopCount++;
                }
                Collections.reverse(mDDList);
                String depart;
                String arrive;
                try {
                    if (mDirectionsResult.routes[0].legs[0].arrivalTime.getMinuteOfHour() < 10) {
                        arrive = mDirectionsResult.routes[0].legs[0].arrivalTime.getHourOfDay() + ":0" + mDirectionsResult.routes[0].legs[0].arrivalTime.getMinuteOfHour();
                    } else {
                        arrive = mDirectionsResult.routes[0].legs[0].arrivalTime.getHourOfDay() + ":" + mDirectionsResult.routes[0].legs[0].arrivalTime.getMinuteOfHour();
                    }
                } catch (Exception e) {
                    if (mDirectionUI.getmDepartureType().equalsIgnoreCase("Arrive")) {
                        DateTime arrival = new DateTime(mDirectionUI.getmDepartTime(), DateTimeZone.forID("Asia/Kuala_Lumpur"));
                        if (arrival.getMinuteOfHour() < 10) {
                            arrive = arrival.getHourOfDay() + ":0" + arrival.getMinuteOfHour();
                        } else {
                            arrive = arrival.getHourOfDay() + ":" + arrival.getMinuteOfHour();
                        }
                    } else {
                        DateTime departure = new DateTime(mDirectionUI.getmDepartTime(), DateTimeZone.forID("Asia/Kuala_Lumpur"));
                        int durationInSecond;
                        if (mDirectionsResult.routes[0].legs[0].steps[0].travelMode.toString().equalsIgnoreCase("WALKING") && mAvgSpeed > 0) {
                            double distance = (double) mDirectionsResult.routes[0].legs[0].steps[0].distance.inMeters;
                            distance = distance / 1000;
                            durationInSecond = (int) ((distance / (mAvgSpeed / 60)) * 60);
                        } else {
                            durationInSecond = (int) mDirectionsResult.routes[0].legs[0].steps[0].duration.inSeconds;
                        }
                        DateTime arrival = departure.minus(durationInSecond);
                        if (arrival.getMinuteOfHour() < 10) {
                            arrive = arrival.getHourOfDay() + ":0" + arrival.getMinuteOfHour();
                        } else {
                            arrive = arrival.getHourOfDay() + ":" + arrival.getMinuteOfHour();
                        }
                    }
                }
                try {
                    if (mLocalDirection.get(mLocalDirection.size() - 1).departureTime.getMinuteOfHour() < 10) {
                        depart = mLocalDirection.get(mLocalDirection.size() - 1).departureTime.getHourOfDay() + ":0" + mLocalDirection.get(mLocalDirection.size() - 1).departureTime.getMinuteOfHour();
                    } else {
                        depart = mLocalDirection.get(mLocalDirection.size() - 1).departureTime.getHourOfDay() + ":" + mLocalDirection.get(mLocalDirection.size() - 1).departureTime.getMinuteOfHour();
                    }
                } catch (Exception e) {
                    if (mDirectionUI.getmDepartureType().equalsIgnoreCase("Arrive")) {
                        DateTime arrival = new DateTime(mDirectionUI.getmDepartTime(), DateTimeZone.forID("Asia/Kuala_Lumpur"));
                        int durationInSecond;
                        if (mDirectionsResult.routes[0].legs[0].steps[0].travelMode.toString().equalsIgnoreCase("WALKING") && mAvgSpeed > 0) {
                            double distance = (double) mDirectionsResult.routes[0].legs[0].steps[0].distance.inMeters;
                            distance = distance / 1000;
                            durationInSecond = (int) ((distance / (mAvgSpeed / 60)) * 60);
                        } else {
                            durationInSecond = (int) mDirectionsResult.routes[0].legs[0].steps[0].duration.inSeconds;
                        }
                        DateTime departure = arrival.minus(durationInSecond);
                        if (departure.getMinuteOfHour() < 10) {
                            depart = departure.getHourOfDay() + ":0" + departure.getMinuteOfHour();
                        } else {
                            depart = departure.getHourOfDay() + ":" + departure.getMinuteOfHour();
                        }
                    } else {
                        DateTime departure = new DateTime(mDirectionUI.getmDepartTime(), DateTimeZone.forID("Asia/Kuala_Lumpur"));
                        if (departure.getMinuteOfHour() < 10) {
                            depart = departure.getHourOfDay() + ":0" + departure.getMinuteOfHour();
                        } else {
                            depart = departure.getHourOfDay() + ":" + departure.getMinuteOfHour();
                        }
                    }
                }

                mDuration = depart + " - " + arrive;

                if (mDirectionsResult.routes[0].legs[0].steps.length > 1) {
                    period = new Period(mLocalDirection.get(mLocalDirection.size() - 1).departureTime, mDirectionsResult.routes[0].legs[0].arrivalTime);
                    mMinutes = period.toStandardMinutes();
                } else {
                    mMinutes = period.toStandardMinutes();
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
        //Normal Now and Depart query
        else {
            try {
                for (int i = 0; i < mDirectionsResult.routes[0].legs[0].steps.length; i++) {
                    DirectionsStep directionsStep = mDirectionsResult.routes[0].legs[0].steps[i];
                    mDirection = directionsStep.htmlInstructions;
                    mDuration = directionsStep.duration.toString();
                    mDistance = directionsStep.distance.toString();

                    DateTime departure;
                    DateTime arrival;

                    int durationInSecond;

                    if (i == 0) {
                        if (mDirectionsResult.routes[0].legs[0].departureTime != null) {
                            departure = mDirectionsResult.routes[0].legs[0].departureTime;
                        } else {
                            departure = new DateTime(mDirectionUI.getmDepartTime(), DateTimeZone.forID("Asia/Kuala_Lumpur"));
                        }


                        if (directionsStep.travelMode.toString().equalsIgnoreCase("WALKING") && mAvgSpeed > 0) {
                            double distance = (double) directionsStep.distance.inMeters;
                            distance = distance / 1000;
                            durationInSecond = (int) ((distance / (mAvgSpeed / 60)) * 60);
                        } else {
                            durationInSecond = (int) directionsStep.duration.inSeconds;
                        }
                        arrival = departure.plusSeconds(durationInSecond);
                        period = new Period(departure, arrival);
                        LocalDirection ld = new LocalDirection(mDirection, mDuration, mDistance, departure, arrival, directionsStep.startLocation, directionsStep.endLocation, "");
                        mLocalDirection.add(ld);
                    } else {
                        departure = mLocalDirection.get(i - 1).arrivalTime;
                        if (mDirectionsResult.routes[0].legs[0].steps[i - 1].travelMode.toString().equalsIgnoreCase("WALKING")) {
                            DirectionsResult customDirectionsResult;
                            customDirectionsResult = customDirection(mLocalDirection.get(i - 1).arrivalTime, directionsStep.startLocation, directionsStep.endLocation);
                            DirectionsStep customDirectionsStep;
                            if (customDirectionsResult.routes[0].legs[0].steps.length == 1) {
                                customDirectionsStep = customDirectionsResult.routes[0].legs[0].steps[0];
                                departure = customDirectionsStep.transitDetails.departureTime;
                            } else if (customDirectionsResult.routes[0].legs[0].steps.length == 0) {
                                throw new Exception();
                            } else {
                                customDirectionsStep = customDirectionsResult.routes[0].legs[0].steps[(customDirectionsResult.routes[0].legs[0].steps.length - 1)];
                                departure = customDirectionsStep.transitDetails.departureTime;
                            }

                            durationInSecond = (int) customDirectionsStep.duration.inSeconds;
                            arrival = departure.plusSeconds(durationInSecond);
                            LocalDirection ld = new LocalDirection(mDirection, mDuration, mDistance, departure, arrival, customDirectionsStep.startLocation, customDirectionsStep.endLocation, customDirectionsStep.transitDetails.departureStop.name);
                            mLocalDirection.add(ld);

                        } else {
                            if (directionsStep.travelMode.toString().equalsIgnoreCase("WALKING") && mAvgSpeed > 0) {
                                double distance = (double) directionsStep.distance.inMeters;
                                distance = distance / 1000;
                                durationInSecond = (int) ((distance / (mAvgSpeed / 60)) * 60);
                            } else {
                                durationInSecond = (int) directionsStep.duration.inSeconds;
                            }
                            arrival = departure.plusSeconds(durationInSecond);
                            LocalDirection ld = new LocalDirection(mDirection, mDuration, mDistance, departure, arrival, directionsStep.startLocation, directionsStep.endLocation, "");
                            mLocalDirection.add(ld);
                        }
                    }

                    SpannableStringBuilder builder = new SpannableStringBuilder();
                    if (directionsStep.travelMode.toString().equalsIgnoreCase("WALKING")) {
                        builder.append(" ", new ImageSpan(this, R.drawable.ic_directions_walk_black_24dp), 0);
                    } else {
                        builder.append(" ", new ImageSpan(this, R.drawable.ic_subway_black_24dp), 0);
                        if (directionsStep.transitDetails.numStops == 1) {
                            mDistance = mDistance + " (" + directionsStep.transitDetails.arrivalStop.name + " - " + directionsStep.transitDetails.numStops + " Stop)";
                        } else {
                            mDistance = mDistance + " (" + directionsStep.transitDetails.arrivalStop.name + " - " + directionsStep.transitDetails.numStops + " Stops)";
                        }
                    }


                    dd = new DirectionData(builder, mDirection, mDistance, ((int) Math.ceil(durationInSecond / 60) + " Mins"), mLocalDirection.get(i).departureTime, mLocalDirection.get(i).arrivalTime, mLocalDirection.get(i).startLocation, mLocalDirection.get(i).endLocation, mLocalDirection.get(i).departureStation);
                    mDDList.add(dd);
                }
                String depart;
                String arrive;
                try {
                    if (mDirectionsResult.routes[0].legs[0].departureTime.getMinuteOfHour() < 10) {
                        depart = mDirectionsResult.routes[0].legs[0].departureTime.getHourOfDay() + ":0" + mDirectionsResult.routes[0].legs[0].departureTime.getMinuteOfHour();
                    } else {
                        depart = mDirectionsResult.routes[0].legs[0].departureTime.getHourOfDay() + ":" + mDirectionsResult.routes[0].legs[0].departureTime.getMinuteOfHour();
                    }
                } catch (Exception e) {
                    if (mDirectionUI.getmDepartureType().equalsIgnoreCase("Arrive")) {
                        DateTime arrival = new DateTime(mDirectionUI.getmDepartTime(), DateTimeZone.forID("Asia/Kuala_Lumpur"));
                        int durationInSecond;
                        if (mDirectionsResult.routes[0].legs[0].steps[0].travelMode.toString().equalsIgnoreCase("WALKING") && mAvgSpeed > 0) {
                            double distance = (double) mDirectionsResult.routes[0].legs[0].steps[0].distance.inMeters;
                            distance = distance / 1000;
                            durationInSecond = (int) ((distance / (mAvgSpeed / 60)) * 60);
                        } else {
                            durationInSecond = (int) mDirectionsResult.routes[0].legs[0].steps[0].duration.inSeconds;
                        }
                        DateTime departure = arrival.minus(durationInSecond);
                        if (departure.getMinuteOfHour() < 10) {
                            depart = departure.getHourOfDay() + ":0" + departure.getMinuteOfHour();
                        } else {
                            depart = departure.getHourOfDay() + ":" + departure.getMinuteOfHour();
                        }
                    } else {
                        DateTime departure = new DateTime(mDirectionUI.getmDepartTime(), DateTimeZone.forID("Asia/Kuala_Lumpur"));
                        if (departure.getMinuteOfHour() < 10) {
                            depart = departure.getHourOfDay() + ":0" + departure.getMinuteOfHour();
                        } else {
                            depart = departure.getHourOfDay() + ":" + departure.getMinuteOfHour();
                        }
                    }
                }
                try {
                    if (mLocalDirection.get(mLocalDirection.size() - 1).arrivalTime.getMinuteOfHour() < 10) {
                        arrive = mLocalDirection.get(mLocalDirection.size() - 1).arrivalTime.getHourOfDay() + ":0" + mLocalDirection.get(mLocalDirection.size() - 1).arrivalTime.getMinuteOfHour();
                    } else {
                        arrive = mLocalDirection.get(mLocalDirection.size() - 1).arrivalTime.getHourOfDay() + ":" + mLocalDirection.get(mLocalDirection.size() - 1).arrivalTime.getMinuteOfHour();
                    }
                } catch (Exception e) {
                    if (mDirectionUI.getmDepartureType().equalsIgnoreCase("Arrive")) {
                        DateTime arrival = new DateTime(mDirectionUI.getmDepartTime(), DateTimeZone.forID("Asia/Kuala_Lumpur"));
                        if (arrival.getMinuteOfHour() < 10) {
                            arrive = arrival.getHourOfDay() + ":0" + arrival.getMinuteOfHour();
                        } else {
                            arrive = arrival.getHourOfDay() + ":" + arrival.getMinuteOfHour();
                        }
                    } else {
                        DateTime departure = new DateTime(mDirectionUI.getmDepartTime(), DateTimeZone.forID("Asia/Kuala_Lumpur"));
                        int durationInSecond;
                        if (mDirectionsResult.routes[0].legs[0].steps[0].travelMode.toString().equalsIgnoreCase("WALKING") && mAvgSpeed > 0) {
                            double distance = (double) mDirectionsResult.routes[0].legs[0].steps[0].distance.inMeters;
                            distance = distance / 1000;
                            durationInSecond = (int) ((distance / (mAvgSpeed / 60)) * 60);
                        } else {
                            durationInSecond = (int) mDirectionsResult.routes[0].legs[0].steps[0].duration.inSeconds;
                        }
                        DateTime arrival = departure.minus(durationInSecond);
                        if (arrival.getMinuteOfHour() < 10) {
                            arrive = arrival.getHourOfDay() + ":0" + arrival.getMinuteOfHour();
                        } else {
                            arrive = arrival.getHourOfDay() + ":" + arrival.getMinuteOfHour();
                        }
                    }
                }
                mDuration = depart + " - " + arrive;

                if (mDirectionsResult.routes[0].legs[0].steps.length > 1) {
                    period = new Period(mDirectionsResult.routes[0].legs[0].departureTime, mLocalDirection.get(mLocalDirection.size() - 1).arrivalTime);
                    mMinutes = period.toStandardMinutes();
                } else {
                    mMinutes = period.toStandardMinutes();
                }
            } catch (Exception e) {
                Toast.makeText(this, "Error searching for direction", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //Convert to unix as google mDirection uses Unix for departure and arrival time
    private Instant convertTimeToInstant(int year, int month, int day, int hour, int minute) {
        DateTimeZone dateTimeZone = DateTimeZone.forID("Asia/Kuala_Lumpur");
        DateTime dateTime = new DateTime(year, month, day, hour, minute, 0, 0, dateTimeZone);

        return dateTime.toInstant();
    }

    private DirectionsResult reverseCustomDirection(DateTime arrivalTime, LatLng origin, LatLng destination) {
        DirectionsResult result = null;
        try {
            GeoApiContext context = new GeoApiContext.Builder().apiKey("AIzaSyBucrA-XR6nEOQ-AMmG3pObvR7oR3eEcYs").build();


            try {
                result = DirectionsApi.newRequest(context).mode(TravelMode.TRANSIT).origin(origin).destination(destination).transitMode(TransitMode.RAIL).arrivalTime(arrivalTime).alternatives(false).await();
                System.out.println(result.routes[0].legs[0].steps[0].htmlInstructions);
                System.out.println(result.routes[0].legs[0].startAddress);
                System.out.println(result.routes[0].legs[0].endAddress);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private DirectionsResult customDirection(DateTime departureTime, LatLng origin, LatLng destination) {
        DirectionsResult result = null;
        try {
            GeoApiContext context = new GeoApiContext.Builder().apiKey("AIzaSyBucrA-XR6nEOQ-AMmG3pObvR7oR3eEcYs").build();


            try {
                result = DirectionsApi.newRequest(context).mode(TravelMode.TRANSIT).origin(origin).destination(destination).transitMode(TransitMode.RAIL).departureTime(departureTime).alternatives(false).await();
            } catch (Exception e) {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    //Launchs mDirection search asynchronously
    private class CustomDirectionAPI extends AsyncTask<URL, Integer, Long> {

        ProgressDialog progressDialog = new ProgressDialog(ActivityDirection.this);

        @Override
        protected void onPreExecute() {
            this.progressDialog.setTitle("Advanced Searching");
            this.progressDialog.setMessage("Searching direction based on your average speed");
            this.progressDialog.setCancelable(false);
            this.progressDialog.show();
        }

        @Override
        protected Long doInBackground(URL... urls) {
            try {
                getDirection();
                }
             catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Long l) {
            mTextDirectionOverview.setText(mDirectionOverview);
            mTextDuration.setText(mDuration);
            mTotalDuration.setText(mMinutes.getMinutes() + "\n Min(s)");
            mList = findViewById(R.id.listDirection);
            DirectionAdapter la = new DirectionAdapter(getBaseContext(), R.layout.direction_list, mDDList);
            mList.setAdapter(la);
            this.progressDialog.dismiss();
        }
    }

    private void scheduleTrip() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        FirebaseAuth auth = FirebaseAuth.getInstance();
        DatabaseReference ref = database.getReference().child("User").child(auth.getCurrentUser().getUid()).child("ScheduleTrip");
        ref.child("origin").setValue(mDirectionsResult.geocodedWaypoints[0].placeId);
        ref.child("destination").setValue(mDirectionsResult.geocodedWaypoints[1].placeId);
        ref.child("departType").setValue(mDirectionUI.getmDepartureType());
        ref.child("distance").setValue(mDirectionsResult.routes[0].legs[0].distance.inMeters);
        ref.child("encodedPath").setValue(mDirectionsResult.routes[0].overviewPolyline.getEncodedPath());
        if (mDirectionUI.getmDepartureType().equalsIgnoreCase("Now")) {
            ref.child("departTime").setValue("");
        } else {
            ref.child("departTime").setValue(mDirectionUI.getmDepartTime().toString());
        }

        Intent intent = getIntent();
        mDirectionUI = (Direction) intent.getSerializableExtra("directionResult");
        mDirectionsResult = mDirectionUI.getmDirectionsResult();

        for (int i = 0; i < mDirectionsResult.routes[0].legs[0].steps.length; i++) {
            DirectionsStep directionsStep = mDirectionsResult.routes[0].legs[0].steps[i];
            mDirection = directionsStep.htmlInstructions;
            mDuration = directionsStep.duration.toString();
            mDistance = directionsStep.distance.toString();

            if (directionsStep.travelMode.toString().equalsIgnoreCase("WALKING")) {
                ref.child("Trip").child("Waypoint_" + i).child("Direction").setValue(mDirection);
                ref.child("Trip").child("Waypoint_" + i).child("TravelMode").setValue(directionsStep.travelMode.toString());
                ref.child("Trip").child("Waypoint_" + i).child("Distance").setValue(mDistance);
                ref.child("Trip").child("Waypoint_" + i).child("Duration").setValue(mDDList.get(i).getmDuration());
                ref.child("Trip").child("Waypoint_" + i).child("Departure Time").setValue(convertTimeToInstant(mDDList.get(i).getmDepartureTime().getYear(), mDDList.get(i).getmDepartureTime().getMonthOfYear(), mDDList.get(i).getmDepartureTime().getDayOfMonth(), mDDList.get(i).getmDepartureTime().getHourOfDay(), mDDList.get(i).getmDepartureTime().getMinuteOfHour()).toString());
                ref.child("Trip").child("Waypoint_" + i).child("Arrival Time").setValue(convertTimeToInstant(mDDList.get(i).getmArrivalTime().getYear(), mDDList.get(i).getmArrivalTime().getMonthOfYear(), mDDList.get(i).getmArrivalTime().getDayOfMonth(), mDDList.get(i).getmArrivalTime().getHourOfDay(), mDDList.get(i).getmArrivalTime().getMinuteOfHour()).toString());
                ref.child("Trip").child("Waypoint_" + i).child("Start Location").setValue(mDDList.get(i).getmStartLocation());
                ref.child("Trip").child("Waypoint_" + i).child("End Location").setValue(mDDList.get(i).getmEndLocation());
            } else {
                ref.child("Trip").child("Waypoint_" + i).child("Direction").setValue(mDirection);
                if (mDirection.contains("Subway towards")){
                    ref.child("Trip").child("Waypoint_" + i).child("Subway Line").setValue(directionsStep.transitDetails.line.shortName);
                }
                ref.child("Trip").child("Waypoint_" + i).child("TravelMode").setValue(directionsStep.travelMode.toString());
                if (directionsStep.transitDetails.numStops == 1) {
                    ref.child("Trip").child("Waypoint_" + i).child("Distance").setValue(mDistance + " (" + directionsStep.transitDetails.arrivalStop.name + " - " + directionsStep.transitDetails.numStops + " Stop)");

                } else {
                    ref.child("Trip").child("Waypoint_" + i).child("Distance").setValue(mDistance + " (" + directionsStep.transitDetails.arrivalStop.name + " - " + directionsStep.transitDetails.numStops + " Stops)");
                }

                ref.child("Trip").child("Waypoint_" + i).child("Duration").setValue(mDuration);
                ref.child("Trip").child("Waypoint_" + i).child("Departure Time").setValue(convertTimeToInstant(mDDList.get(i).getmDepartureTime().getYear(), mDDList.get(i).getmDepartureTime().getMonthOfYear(), mDDList.get(i).getmDepartureTime().getDayOfMonth(), mDDList.get(i).getmDepartureTime().getHourOfDay(), mDDList.get(i).getmDepartureTime().getMinuteOfHour()).toString());
                ref.child("Trip").child("Waypoint_" + i).child("Arrival Time").setValue(convertTimeToInstant(mDDList.get(i).getmArrivalTime().getYear(), mDDList.get(i).getmArrivalTime().getMonthOfYear(), mDDList.get(i).getmArrivalTime().getDayOfMonth(), mDDList.get(i).getmArrivalTime().getHourOfDay(), mDDList.get(i).getmArrivalTime().getMinuteOfHour()).toString());
                ref.child("Trip").child("Waypoint_" + i).child("Start Location").setValue(mDDList.get(i).getmStartLocation());
                ref.child("Trip").child("Waypoint_" + i).child("End Location").setValue(mDDList.get(i).getmEndLocation());
                ref.child("Trip").child("Waypoint_" + i).child("Departure Stop").setValue(mDDList.get(i).getmDepatureStop());
            }
        }

        Intent i = new Intent(getApplicationContext(), TrackTrainService.class);
        getApplicationContext().startService(i);
    }

    //Dataset for custom mDirection query
    private class LocalDirection {

        String direction;
        String distance;
        String duration;
        DateTime departureTime;
        DateTime arrivalTime;
        String departureStation;
        LatLng startLocation;
        LatLng endLocation;

        LocalDirection(String direction, String distance, String duration, DateTime departureTIme, DateTime arrivalTIme, LatLng startLocation, LatLng endLocation, String departureStation) {
            this.direction = direction;
            this.distance = distance;
            this.duration = duration;
            this.departureTime = departureTIme;
            this.arrivalTime = arrivalTIme;
            this.startLocation = startLocation;
            this.endLocation = endLocation;
            this.departureStation = departureStation;
        }

    }
}

package com.tomyamee.smartcommute;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by tommy on 12/07/2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class AccountFragment extends Fragment {

    public static AccountFragment newInstance() {
        return new AccountFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_account, container, false);

        TextView username;
        final TextView speed = view.findViewById(R.id.textWalkStat);
        final TextView level = view.findViewById(R.id.textLevel);
        final ProgressBar progressBar = view.findViewById(R.id.progressLevel);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        username = view.findViewById(R.id.labelUserName);
        if (user != null) {
            username.setText(user.getDisplayName());
        }
        else{
            username.setText("Error");
        }

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        try {
            database.getReference().child("User").child(user.getUid()).child("AverageSpeed").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        String speedString = dataSnapshot.getValue().toString();
                        if (speedString != null) {
                            speed.setText(speedString + " KM/H");
                        } else {
                            speed.setText("No data yet");
                            progressBar.setMax(1000);
                            progressBar.setProgress(0);
                        }
                    }
                    catch(Exception e){
                        speed.setText("No data yet");
                        progressBar.setMax(1000);
                        progressBar.setProgress(0);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            database.getReference().child("User").child(user.getUid()).child("Distance Travelled").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        progressBar.setMax(1000);
                        progressBar.setProgress(0);
                        String distanceString = dataSnapshot.getValue().toString();
                        if (distanceString != null) {
                            Double distanceDouble = Double.valueOf(distanceString);
                            if (distanceDouble > 16000) {
                                level.setText("Level 5" + " - Total distance traveled: " + distanceDouble + " KM");
                                progressBar.setMax(16000);
                            } else if (distanceDouble > 8000) {
                                level.setText("Level 4" + " - Total distance traveled: " + distanceDouble + " KM");
                                progressBar.setMax(16000);
                            } else if (distanceDouble > 4000) {
                                level.setText("Level 3" + " - Total distance traveled: " + distanceDouble + " KM");
                                progressBar.setMax(8000);
                            } else if (distanceDouble > 2000) {
                                level.setText("Level 2" + " - Total distance traveled: " + distanceDouble + " KM");
                                progressBar.setMax(4000);
                            } else if (distanceDouble > 1000) {
                                level.setText("Level 1" + " - Total distance traveled: " + distanceDouble + " KM");
                                progressBar.setMax(2000);
                            } else {
                                level.setText("Level 0" + " - Total distance traveled: " + distanceDouble + " KM");
                                progressBar.setMax(1000);
                            }
                            int converted = (int) Math.floor(distanceDouble);
                            progressBar.setProgress(converted);
                        } else {
                            level.setText("Level 0" + " - Total distance traveled: " + (Double.valueOf(distanceString) / 1000) + " KM");
                            progressBar.setMax(1000);
                            progressBar.setProgress(0);
                        }
                    }
                    catch (Exception e){
                        level.setText("Level 0");
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        catch(Exception e){
            speed.setText("No data yet");
            level.setText("Level 0");
        }

        ArrayList<String> levels = new ArrayList<>();
        levels.add("Level 1 - 5% discount off peak");
        levels.add("Level 2 - Limited edition Touch and Go with 10% off peak trip discount");
        levels.add("Level 3 - 10% discount off peak");
        levels.add("Level 4 - 5% discount for all trip");
        levels.add("Level 5 - Limited edition Touch and Go with 10% discount for all trip");

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, levels);
        ListView listView = view.findViewById(R.id.listProgression);
        listView.setAdapter(arrayAdapter);


        Button button = view.findViewById(R.id.buttonLogOut);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    ((MainActivity) getActivity()).startSignOut();
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        return view;
    }
}

# SmartCommute Android App

**SmartCommute** is a public train commuting planning application is mainly targeted to **users who commute to places by public trains**. This platform is aimed to allow users to *carefully plan and schedule their trips* so that they will not be afraid of missing trains arrival and departure time. Proper planning also allows user to identify and understand the things to do and accomplish within a specific time-frame. This can helps keep users on track and not be late or miss out things to do.

## Features

 - Login with Firebase Authentication [Email and Google support]
 - Search for trip from Point A to Point B
 - Custom User Walking Speed for customized and more accurate ETA
 - Automatic trip selection for the most optimal route with fastest and shortest route possible
 - Navigate for trip from Point A to Point B
 - Track live location of train [Simulated with dummy data]
 - Notify user on train delay through notification [Does not bypass Android Doze Mode]

## API Used

 - [Firebase Authentication](https://firebase.google.com/products/auth/)
 - [Firebase Database](https://firebase.google.com/products/realtime-database/)
 - [Google Map API for Android](https://developers.google.com/maps/documentation/android-api/start)
 - [Google Map Places for Android](https://developers.google.com/places/android-api/start)
 - [Google Map Direction API](https://developers.google.com/maps/documentation/directions/start)
 - [Google Map Geocoding API](https://developers.google.com/maps/documentation/geocoding/start)
 - [Java Client for Google Maps Services](https://github.com/googlemaps/google-maps-services-java) [Direction, Geocoding]
 - [RecyclerViewPager Custom Library](https://github.com/lsjwzh/RecyclerViewPager)


